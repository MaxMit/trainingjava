package lv.sda.utils;

import java.util.List;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;

public class UserInputService {

    public static String getUserInput(String messageToUser) {
        System.out.println(messageToUser);
        Scanner sc = new Scanner(System.in);
        return sc.nextLine();
    }

    public static Object getUserInputMultiple(String questionToUser, List<String> possibleChoices) {
        final AtomicInteger i = new AtomicInteger(0);
        System.out.println(questionToUser);
        System.out.println("***********************");
        possibleChoices.forEach(s -> {
            System.out.println(i + ". " + s);
            i.incrementAndGet();
        });
        System.out.println("***********************");

        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }
}
