package lv.sda.utils;

import lv.sda.entity.Course;
import lv.sda.entity.Feedback;
import lv.sda.entity.Trainer;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;

import java.util.Properties;

public class HibernateUtils {
    private static SessionFactory sessionFactory;

    public static SessionFactory getSessionFactory() {

        if (sessionFactory == null) {

            try {
                Configuration configuration = new Configuration();
                Properties settings = new Properties();
                settings.put(Environment.DRIVER, "com.mysql.jdbc.Driver");
                settings.put(Environment.URL,
                        "jdbc:mysql://ec2-13-53-152-91.eu-north-1.compute.amazonaws.com:3306/sda?useLegacyDatetimeCode=false&serverTimezone=UTC");
                settings.put(Environment.USER, "sdacademy");
                settings.put(Environment.PASS, "808080cba");
                settings.put(Environment.DIALECT,
                        "org.hibernate.dialect.MySQL5Dialect");
                settings.put(Environment.SHOW_SQL, "true");
                configuration.setProperties(settings);

                configuration.addAnnotatedClass(Trainer.class); //AUTOMATED in IoC (Spring)
                configuration.addAnnotatedClass(Course.class);
                configuration.addAnnotatedClass(Feedback.class);

                ServiceRegistry serviceRegistry = new
                        StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
                sessionFactory =
                        configuration.buildSessionFactory(serviceRegistry);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return sessionFactory;
    }
}
