package lv.sda.dao;


import lv.sda.entity.Course;
import lv.sda.entity.Trainer;
import lv.sda.utils.HibernateUtils;

import java.util.List;

public class TrainerDao extends AbstractDao<Trainer> {

    public static Trainer findById(Integer trainerId) {
        return HibernateUtils.getSessionFactory().openSession().get(Trainer.class, trainerId);
    }

    public static List<Trainer> findAll() {
        List<Trainer> trainers = (List<Trainer>) HibernateUtils.getSessionFactory().openSession().createQuery("From Trainer").list();
        return trainers;
    }
}