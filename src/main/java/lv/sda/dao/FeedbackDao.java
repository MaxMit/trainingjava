package lv.sda.dao;


import lv.sda.entity.Feedback;
import lv.sda.utils.HibernateUtils;

import java.util.List;

public class FeedbackDao extends AbstractDao<Feedback> {

    public static Feedback findById(Integer trainerId) {
        return HibernateUtils.getSessionFactory().openSession().get(Feedback.class, trainerId);
    }

    public static List<Feedback> findAll() {
        List<Feedback> trainers = (List<Feedback>) HibernateUtils.getSessionFactory().openSession().createQuery("From Feedback").list();
        return trainers;
    }
}