package lv.sda.dao;

import lv.sda.utils.HibernateUtils;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class AbstractDao<T> {

    public void save(T trainer) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(trainer);
        tx1.commit();
        session.close();
    }

    public void update(T trainer) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(trainer);
        tx1.commit();
        session.close();
    }

    public void delete(T trainer) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.delete(trainer);
        tx1.commit();
        session.close();
    }



}
