package lv.sda.dao;


import lv.sda.entity.Course;
import lv.sda.entity.Trainer;
import lv.sda.utils.HibernateUtils;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class CourseDao extends AbstractDao<Course> {

    public static Course findById(Integer trainerId) {
        return HibernateUtils.getSessionFactory().openSession().get(Course.class, trainerId);
    }

    public static List<Course> findAll() {
        List<Course> trainers = (List<Course>) HibernateUtils.getSessionFactory().openSession().createQuery("From Course").list();
        return trainers;
    }

}