package lv.sda.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import lv.sda.dao.TrainerDao;
import lv.sda.entity.Trainer;

import java.util.List;

public class TrainingController {

    @FXML
    private TableView<Trainer> trainersTable;

    @FXML
    TextField trainerFirstName;

    @FXML
    private TextField trainerLastName;

    @FXML
    private TextField trainerEmail;

    @FXML
    private TableColumn<Trainer, Integer> trainerIdColumn;

    @FXML
    private TableColumn<Trainer, String> trainerFirstNameColumn;

    @FXML
    private TableColumn<Trainer, String> trainerLastNamecolumn;

    @FXML
    private TableColumn<Trainer, String> trainerEmailColumn;
    private TrainerDao trainerDao = new TrainerDao();

    @FXML
    void trainersTableClicked(MouseEvent event) {
        Trainer selected = trainersTable.getSelectionModel().getSelectedItem();
        trainerFirstName.setText(selected.getFirstName());
        trainerLastName.setText(selected.getLastName());
        trainerEmail.setText(selected.getEmail());
    }

    @FXML
    void fetchTrainers(ActionEvent event) {
        trainerIdColumn.setCellValueFactory(new PropertyValueFactory<>("trainerId"));
        trainerFirstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        trainerLastNamecolumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        trainerEmailColumn.setCellValueFactory(new PropertyValueFactory<>("email"));
        trainersTable.getItems().clear();
        List<Trainer> trainer = TrainerDao.findAll();
        trainersTable.getItems().addAll(trainer);

    }

    @FXML
    protected void addTrainer(ActionEvent event) {
        Trainer p = new Trainer();
        p.setFirstName(trainerFirstName.getText());
        p.setLastName(trainerLastName.getText());
        p.setEmail(trainerEmail.getText());
        trainerDao.save(p);
        System.out.println("Trainer saved");
    }

    @FXML
    protected void updateTrainer(ActionEvent event) {
        Trainer selected = trainersTable.getSelectionModel().getSelectedItem();
        Trainer updatedTrainer = new Trainer();
        updatedTrainer.setTrainerId(selected.getTrainerId());
        updatedTrainer.setFirstName(trainerFirstName.getText());
        updatedTrainer.setLastName(trainerLastName.getText());
        updatedTrainer.setEmail(trainerEmail.getText());
        trainerDao.update(updatedTrainer);
        System.out.println("Trainer updated");
        fetchTrainers(new ActionEvent());

    }

    @FXML
    protected void deleteTrainer(ActionEvent event) {
        Trainer selected = trainersTable.getSelectionModel().getSelectedItem();
        Trainer toDelete = new Trainer();
        toDelete.setTrainerId(selected.getTrainerId());
        trainerDao.delete(toDelete);
        fetchTrainers(new ActionEvent());
        System.out.println("Trainer deleted");
    }
}
