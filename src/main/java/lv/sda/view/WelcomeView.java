package lv.sda.view;

import lv.sda.utils.UserInputService;

import java.util.ArrayList;
import java.util.List;

public class WelcomeView {
    public static int displayMainMenu() {
        List<String> choices = new ArrayList<>();
        choices.add("Add Trainer");
        choices.add("Add Courses");
        return  Integer.parseInt(
                UserInputService.getUserInputMultiple("What would you like to do?", choices).toString()
        );
    }

}
