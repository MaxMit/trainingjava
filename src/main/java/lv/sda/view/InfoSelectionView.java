package lv.sda.view;

import lv.sda.utils.UserInputService;

public class InfoSelectionView {
    public static String askFirstName() {
        return UserInputService.getUserInput("Enter trainer's firstName");
    }
    public static String askLastName() {
        return UserInputService.getUserInput("Enter trainer's lastName");
    }
    public static String askEmail() {
        return UserInputService.getUserInput("Enter trainer's email");
    }
}
