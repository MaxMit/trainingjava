package lv.sda.services;

import lv.sda.dao.TrainerDao;
import lv.sda.entity.Course;
import lv.sda.entity.Trainer;

import java.util.List;

public class TrainerService {

    private TrainerDao trainersDao = new TrainerDao();

    public TrainerService() {
    }

    public Trainer findTrainer(Integer trainerId) {
        return TrainerDao.findById(trainerId);
    }

    public void saveTrainer(Trainer trainer) {
        trainersDao.save(trainer);
    }

    public void deleteTrainer(Trainer trainer) {
        trainersDao.delete(trainer);
    }

    public void updateTrainer(Trainer trainer) {
        trainersDao.update(trainer);
    }

    public List<Trainer> findAllTrainers() {
        return trainersDao.findAll();
    }
}