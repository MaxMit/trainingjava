package lv.sda.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "lions_trainers")
public class Trainer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Integer trainerId;
    private String firstName;
    private String lastName;
    private String email;

    @OneToMany(mappedBy = "trainer", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Course> courses;

    public Trainer() {
    }

    public Trainer(String firstName, String lastName, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        courses = new ArrayList<>();
    }

    public void addCourse(Course course) {
        course.setTrainer(this);
        courses.add(course);
    }

    public void removeAuto(Course course) {
        courses.remove(course);
    }

    public List<Course> getAutos() {
        return courses;
    }

    public void setAutos(List<Course> courses) {
        this.courses = courses;
    }

    public Integer getTrainerId() {
        return trainerId;
    }

    public void setTrainerId(Integer trainerId) {
        this.trainerId = trainerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    @Override
    public String toString() {
        return "lv.sda.entity.Trainer{" +
                "trainerId=" + trainerId +
                ", firstName='" + firstName + " lastName='" + lastName +'\'' +
                ", email=" + email +
                '}';
    }
}
