package sda;

import lv.sda.dao.CourseDao;
import lv.sda.dao.TrainerDao;
import lv.sda.entity.Course;
import lv.sda.entity.Trainer;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class CourseDaoTest {

    @Test
    public void testFindById() {
        Course course = CourseDao.findById(1);
        assertThat(course.getCourseId(), is(1));
    }

    @Test
    public void testFindAll() {
        List<Course> courses = CourseDao.findAll();
        assertNotNull(courses);
        assertTrue(courses.size()>0);
    }
}
