package sda;

import lv.sda.dao.FeedbackDao;
import lv.sda.dao.TrainerDao;
import lv.sda.entity.Feedback;
import lv.sda.entity.Trainer;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class TrainerDaoTest {

    @Test
    public void testFindById() {
        Trainer trainer = TrainerDao.findById(2);
        assertThat(trainer.getTrainerId(), is(2));
    }

    @Test
    public void testFindAll() {
        List<Trainer> courses = TrainerDao.findAll();
        assertNotNull(courses);
        assertTrue(courses.size()>0);
    }
}
