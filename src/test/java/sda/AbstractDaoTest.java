package sda;

import lv.sda.dao.AbstractDao;
import lv.sda.dao.TrainerDao;
import lv.sda.entity.Trainer;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class AbstractDaoTest {
    @Test
    public void testTrainerSaveAndUpdate() {
        AbstractDao<Trainer> ad = new AbstractDao<>();
        Trainer p = new Trainer();
        p.setFirstName("firstName");
        p.setLastName("lastName");
        p.setEmail("email");
        ad.save(p);
        assertNotNull(p.getTrainerId());
        p.setFirstName("firstName updated");
        p.setLastName("lastName updated");
        p.setEmail("email updated");
        ad.update(p);
        Trainer trainer = TrainerDao.findById(p.getTrainerId());
        assertThat(trainer.getFirstName(), is("firstName updated"));
        assertThat(trainer.getLastName(), is("lastName updated"));
        assertThat(trainer.getEmail(), is("email updated"));
    }

    @Test
    public void testTrainerDelete() {
        Trainer p = new Trainer();
        AbstractDao<Trainer> ad = new AbstractDao<>();
        p.setFirstName("firstName");
        p.setLastName("lastName");
        p.setEmail("email");
        ad.save(p);
        assertNotNull(p.getTrainerId());
        ad.delete(p);
        Trainer trainer = TrainerDao.findById(p.getTrainerId());
        assertNull(trainer);
    }
}
