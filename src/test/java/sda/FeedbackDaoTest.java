package sda;

import lv.sda.dao.CourseDao;
import lv.sda.dao.FeedbackDao;
import lv.sda.dao.TrainerDao;
import lv.sda.entity.Course;
import lv.sda.entity.Feedback;
import lv.sda.entity.Trainer;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class FeedbackDaoTest {

    @Test
    public void testFindById() {
        Feedback course = FeedbackDao.findById(1);
        assertThat(course.getFeedbackId(), is(1));
    }

    @Test
    public void testFindAll() {
        List<Feedback> courses = FeedbackDao.findAll();
        assertNotNull(courses);
        assertTrue(courses.size()>0);
    }
}
